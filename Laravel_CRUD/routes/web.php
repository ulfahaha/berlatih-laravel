<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/table', function(){
    return view('page.table');
});

route::get('/data-tables', function(){
    return view('page.data_table');
});

//Laravel CRUD

route::get('/cast', 'castController@index'); //menampilkan berdasar table

route::get('/cast/create', 'castController@create'); //membuat isian cast

route::post('/cast', 'castController@store'); //menyimpan isian cast yang sudah dibuat

route::get('/cast/{cast_id}', 'castController@show'); //menampilkan berdasar id

route::get('/cast/{cast_id}/edit', 'castController@edit'); //edit data

route::put('/cast/{cast_id}', 'castController@update'); //update data yang telah di edit

route::delete('/cast/{cast_id}', 'castController@destroy'); //hapus data