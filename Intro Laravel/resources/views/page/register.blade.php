<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Form</title>
</head>

<body>
    <form action="/welcome" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>

        <label>First Name :</label><br>
        <input type="text" name="FN">
        <br><br>

        <label>Last Name :</label><br>
        <input type="text" name="LN">
        <br><br>

        <label>Gender :</label><br>
        <input type="radio" name="Gender">Male <br>
        <input type="radio" name="Gender">Female <br>
        <input type="radio" name="Gender">Other
        <br><br>

        <label>Nationality :</label><br>
            <select name="Nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="Singapuran">Singapuran</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select>
        <br><br>

        <label>Language Spoken :</label><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other
        <br><br>

        <label>Bio :</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <br>

        <input type="submit" value="Sign Up">
    </form>
</body>

</html>