<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('page.register');
    }

    public function selamatDatang(Request $request) {
        $namaDepan = $request->FN;
        $namaBelakang = $request->LN;
        return view('page.welcome', compact('namaDepan', 'namaBelakang'));
    }
}
